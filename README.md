

# Animator utility

__*NOTE: be sure to turn off css transition option as it will break this animator!*__

## Cross fade example

```
let transitioning = false;
const transition() => {
  if (!transitioning) {
    transitioning = true;

    Animator.transition(1, 0, 400, (val) => {
      element.style.opacity = val;
    }, () => {
      Animator.transition(0, 1, 400, (val) => {
        
      current_element.style.display = 'none';
.style.opacity = val;
      }, () => {
        transitioning = false;;
      });
    });
  }
}
```
