
export default class Animator {
  static transition(element, from, to, duration, vec_callback, done_callback) {
    if (!element.animator) {
      element.animator = { duration: duration };
      const start_time = element.animator.start_time = Date.now();
      const linear_vector = (from - to) * -1;
      element.animator.linear_vector = linear_vector;
      let tns_intv = element.animator.interval = setInterval(() => {
        if (!element.animator) {
          element.animator = {
            duration: duration,
            linear_vector: linear_vector
          };
        }
        const current_time = Date.now();
        const delta = element.animator.delta = current_time - start_time;
        const multiplier = delta / duration;
        if (current_time > start_time + duration) {
          clearInterval(tns_intv);
          let value = to;
          vec_callback(value);
          delete element.animator;
          done_callback();
          return;
        } else {
          let value = from + linear_vector * multiplier; // linear
          vec_callback(value);
        }
      }, 1);
    } else {
      if (Date.now() - element.animator.start_time < 100) return;
      clearInterval(element.animator.interval);
      if (element.animator.linear_vector < 0) {
        duration -= element.animator.delta;
      } else {
        duration -= element.animator.duration - element.animator.delta;
      }
      if (duration < 0 || isNaN(duration)) duration = 0;
      delete element.animator;
      Animator.transition(element, from, to, duration, vec_callback, done_callback);
    }
  }

}

